# Mandelbrot

[Wikipeadia](https://en.wikipedia.org/wiki/Mandelbrot_set) has a good discretion of the Mandelbrot set. It is a fractal patten that is quite pretty.

## Navigation

Both the Cli and gui allow you to locate your bot in the complex plane and a zoom level. X is used for the real part and Y for the complex, and the zoom is given as the magnitude of the vector from the center to the minimum dimension, it is 2 to see the hole set.

In the gui you can use the arrow keys to move and `+` and `-` to zoom

You can click to re-center the bot under your curser and scroll the middle mouse button to zoom.

The mouse can also click and drag to zoom in on a area of the visible set and clicking and dragging the left mouse while the ctl key is pressed
results in the view panning with the mouse.

## TUI

When running from source

``` bash
   cargo run --release -- -r 51
```

## GUI

When running from source

``` bash
    cargo run --release -- -g
```
