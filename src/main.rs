use clap::{App, Arg};
use num::complex::Complex64;
use num_cpus;
use std::sync::mpsc;
use std::sync::mpsc::{Receiver, Sender};
use std::{ops::Mul, sync::Arc, thread};
use tracing::instrument;

use druid::text::ParseFormatter;
use druid::{
    keyboard_types,
    piet::{ImageBuf, ImageFormat, InterpolationMode},
    widget::prelude::*,
    widget::{CrossAxisAlignment, Flex, Label, RadioGroup, TextBox, WidgetExt},
    Affine, AppDelegate, AppLauncher, BoxConstraints, Color, Command, Data, DelegateCtx, Env,
    Event, EventCtx, ExtEventSink, Handled, LayoutCtx, Lens, LifeCycle, LifeCycleCtx,
    LocalizedString, PaintCtx, Point, Rect, Selector, Size, Target, UpdateCtx, Widget, WindowDesc,
};
use rayon::prelude::*;

mod colormap;
use colormap::ViridisMap;

use crate::colormap::ColorMapping;

#[derive(PartialEq)]
struct BotDetails {
    width: usize,
    height: usize,
    center_x: f64,
    center_y: f64,
    rad: f64,
}
struct Bot {
    values: Vec<Option<u32>>,
    details: BotDetails,
}

const FINISH_SLOW_FUNCTION: Selector<Arc<Bot>> = Selector::new("finish_slow_function");
const REQUEST_SLOW_FUNCTION: Selector<NewBot> = Selector::new("request_slow_function");

impl PartialEq for Bot {
    fn eq(&self, other: &Self) -> bool {
        self.details == other.details
    }
}

struct BotImage {
    image: ImageBuf,
    center_x: f64,
    center_y: f64,
    rad: f64,
}

#[derive(Copy, Clone, PartialEq, Data)]
enum ColourScale {
    Log,
    Linear,
}

impl ColourScale {
    fn scale(&self, input: f64) -> f64 {
        match self {
            ColourScale::Log => input.log10(),
            ColourScale::Linear => input,
        }
    }
}

static COLOUR_SCALE_OPTIONS: &[(&str, ColourScale)] =
    &[("Log", ColourScale::Log), ("Linear", ColourScale::Linear)];

fn mandrel(previous: Complex64, base: Complex64) -> Complex64 {
    previous.mul(previous) + base
}

fn is_bot(input: &Complex64, max_iter: u32) -> Option<u32> {
    if input.norm() > 2.0 {
        return Some(0);
    }
    let c = input.to_owned();
    let mut result: Complex64 = c;
    let mut last = Complex64::new(0.0, 0.0);
    for iii in 0..max_iter {
        result = mandrel(result, c);
        if result.re.mul(result.re) + result.im.mul(result.im) > 4.0 {
            return Some(iii);
        }
        if result.eq(&last) {
            return None;
        };
        last = result;
    }
    None
}

#[derive(Copy, Clone)]
struct NewBot {
    sub_divs: i32,
    x_center: f64,
    y_center: f64,
    scale: f64,
    max_iter: u32,
}

fn do_bot_slow(new: NewBot) -> Bot {
    let mut raw_input = vec![];
    for row in 0..new.sub_divs {
        let im_f = row as f64 / (new.sub_divs - 1) as f64;
        let im = im_f * 2.0 * new.scale - new.scale + new.y_center;

        for col in 0..(new.sub_divs) {
            let re_f = col as f64 / (new.sub_divs - 1) as f64;
            let re = re_f * 2.0 * new.scale - new.scale + new.x_center;
            raw_input.push(Complex64::new(re, im))
        }
    }

    let raw_bot = raw_input
        .iter()
        .map(|num| is_bot(num, new.max_iter))
        .collect();

    Bot {
        values: raw_bot,
        details: BotDetails {
            width: new.sub_divs as usize,
            height: new.sub_divs as usize,
            center_x: new.x_center,
            center_y: new.y_center,
            rad: new.scale,
        },
    }
}

fn do_bot(new: NewBot) -> Bot {
    let mut raw_input = vec![];
    for row in 0..new.sub_divs {
        let im_f = row as f64 / (new.sub_divs - 1) as f64;
        let im = im_f * 2.0 * new.scale - new.scale + new.y_center;

        for col in 0..(new.sub_divs) {
            let re_f = col as f64 / (new.sub_divs - 1) as f64;
            let re = re_f * 2.0 * new.scale - new.scale + new.x_center;
            raw_input.push(Complex64::new(re, im))
        }
    }

    let raw_bot = raw_input
        .par_iter()
        .map(|num| is_bot(num, new.max_iter))
        .collect();

    Bot {
        values: raw_bot,
        details: BotDetails {
            width: new.sub_divs as usize,
            height: new.sub_divs as usize,
            center_x: new.x_center,
            center_y: new.y_center,
            rad: new.scale,
        },
    }
}

fn print_bot(bot: Bot) {
    for row in 0..(bot.details.width) {
        for col in 0..(bot.details.height) {
            if bot.values[row * bot.details.height + col].is_some() {
                print!(" ");
            } else {
                print!("B");
            }
        }
        println!("");
    }
}

struct ColourSettings {
    max: f64,
    min: f64,
    colour: ColourScale,
    alpha: bool,
}

fn bot_to_buff(bot: &Bot, colormap: &ViridisMap, colour_settings: ColourSettings) -> BotImage {
    //let max_val = bot.values.iter().map(|&v| {v.get_or_insert(0)}).max().unwrap();
    //let max_val = max_val.to_owned().Copy() as f64;

    let max_val = colour_settings.max as f64;
    let mut raw: Vec<u8> = Vec::with_capacity(bot.values.len());
    let max = colour_settings.colour.scale(1.0);
    let min = colour_settings
        .colour
        .scale((colour_settings.min as f64 + 1.0) / (max_val + 1.0));
    let alpha = if colour_settings.alpha { 100 } else { 255 };
    for val in &bot.values {
        match val {
            Some(dist) => {
                let ratio = (dist.to_owned() as f64 + 1.0) / (max_val + 1.0);

                let r_10 = colour_settings.colour.scale(ratio);
                let r_n = (r_10 - min) / (max - min);
                //let intens = 255 - (254.0 * ratio).min(254.0) as u8;
                //print!{" {}-{}-{} ", dist, ratio, intens}
                let (r, g, b) = colormap.get_color(1.0 - r_n);
                raw.push(r);
                raw.push(g);
                raw.push(b);
                raw.push(alpha);
            }
            None => {
                raw.push(0);
                raw.push(0);
                raw.push(0);
                raw.push(alpha);
            }
        };
    }

    let buff = ImageBuf::from_raw(
        raw,
        ImageFormat::RgbaSeparate,
        bot.details.width,
        bot.details.height,
    );

    BotImage {
        image: buff,
        center_x: bot.details.center_x,
        center_y: bot.details.center_y,
        rad: bot.details.rad,
    }
}

pub struct DruidBotWidget {
    image_vis: BotImage,
    image_vis_a: BotImage,
    image_qual: Option<BotImage>,
    interpolation: InterpolationMode,
    colormap: ViridisMap,
    mouse_down: Option<Point>,
    mouse_current: Option<Point>,
    mouse_ctl: Option<bool>,
}

#[derive(Clone, Lens, Data, PartialEq)]
pub struct BotData {
    bot_qual: Option<Arc<Bot>>,

    // this reflects what is on the view
    view_box: Rect,

    center_x: f64,
    center_y: f64,

    view: f64,

    min: u32,
    max: u32,

    colour_scale: ColourScale,

    data_processing: bool,
}

fn wrapped_bot_render(sink: ExtEventSink, bot_details: NewBot) {
    sink.submit_command(REQUEST_SLOW_FUNCTION, bot_details, Target::Auto)
        .expect("command failed to submit");
}

impl DruidBotWidget {
    /// Create an image drawing widget from an image buffer.
    ///
    /// By default, the Image will scale to fit its box constraints ([`FillStrat::Fill`])
    /// and will be scaled bilinearly ([`InterpolationMode::Bilinear`])
    ///
    /// The underlying `ImageBuf` uses `Arc` for buffer data, making it cheap to clone.
    ///
    /// [`FillStrat::Fill`]: crate::widget::FillStrat::Fill
    /// [`InterpolationMode::Bilinear`]: crate::piet::InterpolationMode::Bilinear
    #[inline]
    pub fn new() -> Self {
        let bot = do_bot(NewBot {
            sub_divs: 100,
            x_center: 0.0,
            y_center: 0.0,
            scale: 8.0,
            max_iter: 1000,
        });
        let cm = ViridisMap::new();
        let colour_settings = ColourSettings {
            max: 1000.0,
            min: 1.0,
            colour: ColourScale::Log,
            alpha: false,
        };
        let colour_settings_a = ColourSettings {
            max: 1000.0,
            min: 1.0,
            colour: ColourScale::Log,
            alpha: true,
        };

        DruidBotWidget {
            image_vis: bot_to_buff(&bot, &cm, colour_settings),
            image_vis_a: bot_to_buff(&bot, &cm, colour_settings_a),
            image_qual: None,
            interpolation: InterpolationMode::Bilinear,
            colormap: cm,
            mouse_down: None,
            mouse_current: None,
            mouse_ctl: None,
        }
    }

    /// Builder-style method for specifying the interpolation strategy.
    #[inline]
    pub fn interpolation_mode(mut self, interpolation: InterpolationMode) -> Self {
        self.interpolation = interpolation;
        // Invalidation not necessary
        self
    }

    /// Modify the widget's interpolation mode.
    #[inline]
    pub fn set_interpolation_mode(&mut self, interpolation: InterpolationMode) {
        self.interpolation = interpolation;
        // Invalidation not necessary
    }
}

fn to_pix_per_view(size: Size, view: f64) -> f64 {
    let parent_x = size.width;
    let parent_y = size.height;
    let parent_min = parent_x.min(parent_y);
    parent_min / (view * 2.0)
}

fn to_mid(point: Point, view: Size, pixels_per_view: f64) -> Point {
    let x_raw = point.x - (view.width / 2.0);
    let y_raw = point.y - (view.height / 2.0);

    let x_p = x_raw / pixels_per_view;
    let y_p = y_raw / pixels_per_view;
    Point::new(x_p, y_p)
}

impl Widget<BotData> for DruidBotWidget {
    #[instrument(skip(self, ctx, data, _env))]
    fn event(&mut self, ctx: &mut EventCtx, event: &Event, data: &mut BotData, _env: &Env) {
        if !ctx.has_focus() {
            ctx.request_focus();
        };
        match event {
            Event::MouseDown(mouse) => {
                self.mouse_down = Some(mouse.pos.to_owned());
                self.mouse_ctl = Some(mouse.mods.ctrl());
                ctx.request_paint();
            }
            Event::MouseMove(mouse) => {
                self.mouse_current = Some(mouse.pos.to_owned());
                match self.mouse_ctl {
                    Some(ctl) if ctl => {
                        let pixels_per_view = to_pix_per_view(ctx.size(), data.view);
                        let mouse_as_view_now = to_mid(mouse.pos, ctx.size(), pixels_per_view);

                        match self.mouse_down {
                            Some(mouse_start) if mouse_start.ne(&mouse.pos) => {
                                let mouse_as_view_start =
                                    to_mid(mouse_start, ctx.size(), pixels_per_view);
                                data.center_y += mouse_as_view_start.y - mouse_as_view_now.y;
                                data.center_x += mouse_as_view_start.x - mouse_as_view_now.x;
                                self.mouse_down = Some(mouse.pos);
                            }
                            _ => {}
                        }
                    }
                    _ => {
                        ctx.request_paint();
                    }
                }
            }
            Event::MouseUp(mouse) => {
                match self.mouse_ctl {
                    Some(ctl) if !ctl => {
                        let pixels_per_view = to_pix_per_view(ctx.size(), data.view);
                        let mouse_as_view_now = to_mid(mouse.pos, ctx.size(), pixels_per_view);
                        match self.mouse_down {
                            Some(mouse_start) if mouse_start.ne(&mouse.pos) => {
                                let mouse_as_view_start =
                                    to_mid(mouse_start, ctx.size(), pixels_per_view);
                                data.center_y +=
                                    (mouse_as_view_start.y + mouse_as_view_now.y) / 2.0;
                                data.center_x +=
                                    (mouse_as_view_start.x + mouse_as_view_now.x) / 2.0;
                                data.view = (mouse_as_view_now.y - mouse_as_view_start.y)
                                    .abs()
                                    .max((mouse_as_view_now.x - mouse_as_view_start.x).abs())
                                    / 2.0;
                            }
                            _ => {
                                data.center_y += mouse_as_view_now.y;
                                data.center_x += mouse_as_view_now.x;
                            }
                        }
                    }
                    _ => {}
                };
                self.mouse_current = None;
                self.mouse_down = None;
                self.mouse_ctl = None;
                ctx.request_paint();
            }
            Event::Wheel(mouse) => {
                let scale = if mouse.mods.ctrl() { 0.1 } else { 0.01 };
                if mouse.wheel_delta.y < 0.0 {
                    data.view -= data.view * scale;
                } else {
                    data.view += data.view * scale;
                }
            }
            Event::KeyDown(key) => match &key.key {
                keyboard_types::Key::ArrowUp => data.center_y -= data.view * 0.01,
                keyboard_types::Key::ArrowDown => data.center_y += data.view * 0.01,
                keyboard_types::Key::ArrowLeft => data.center_x -= data.view * 0.01,
                keyboard_types::Key::ArrowRight => data.center_x += data.view * 0.01,
                keyboard_types::Key::Character(s) if s == &"+".to_string() => {
                    data.view -= data.view * 0.01;
                }
                keyboard_types::Key::Character(s) if s == &"-".to_string() => {
                    data.view += data.view * 0.01;
                }
                _ => {}
            },
            _ => {}
        }
    }

    fn lifecycle(
        &mut self,
        _ctx: &mut LifeCycleCtx,
        event: &LifeCycle,
        _data: &BotData,
        _env: &Env,
    ) {
        match event {
            LifeCycle::HotChanged(_) => {
                self.mouse_current = None;
                self.mouse_down = None;
                self.mouse_ctl = None;
            }
            _ => {}
        }
    }
    #[instrument(name = "bot", level = "trace", skip(self, ctx, old_data, data, _env))]
    fn update(&mut self, ctx: &mut UpdateCtx, old_data: &BotData, data: &BotData, _env: &Env) {
        if old_data.ne(data) {
            let scale_fac = ctx.size().max_side() / ctx.size().min_side();
            let pixels = (ctx.size().min_side() * scale_fac * 0.05) as i32;
            let bot_zoom = do_bot_slow(NewBot {
                sub_divs: pixels,
                x_center: data.center_x,
                y_center: data.center_y,
                scale: data.view * scale_fac,
                max_iter: 100,
            });

            let colour_settings = ColourSettings {
                max: data.max as f64,
                min: data.min as f64,
                colour: data.colour_scale,
                alpha: false,
            };
            self.image_vis_a = bot_to_buff(&bot_zoom, &self.colormap, colour_settings);
            let colour_settings_a = ColourSettings {
                max: data.max as f64,
                min: data.min as f64,
                colour: data.colour_scale,
                alpha: true,
            };
            self.image_vis = bot_to_buff(&bot_zoom, &self.colormap, colour_settings_a);

            // Due to the size of `data.bot_qual` this is still too slow and needs moving
            // in to the queue system. we should use that as a opportunity to also have good
            // logic for when a `BotImage` is equal to what would be produced from current `data`
            // so superfluous `bot_to_buffs` can be skipped.

            // This check is sensible but dose not take colour map in to account
            if old_data.bot_qual != data.bot_qual {
                if let Some(bot) = &data.bot_qual {
                    let colour_settings = ColourSettings {
                        max: data.max as f64,
                        min: data.min as f64,
                        colour: data.colour_scale,
                        alpha: false,
                    };
                    self.image_qual = Some(bot_to_buff(&bot, &self.colormap, colour_settings));
                }
            };

            //if changed && data.data_processing == false {
            let scale_fac = ctx.size().max_side() / ctx.size().min_side();
            let pixels = (ctx.size().min_side() * scale_fac) as i32;
            //data.data_processing = true;

            wrapped_bot_render(
                ctx.get_external_handle(),
                NewBot {
                    sub_divs: pixels,
                    x_center: data.center_x,
                    y_center: data.center_y,
                    scale: data.view * scale_fac,
                    max_iter: 5000,
                },
            );
            ctx.request_paint();
            ctx.request_layout();
        }
    }

    fn layout(
        &mut self,
        _layout_ctx: &mut LayoutCtx,
        bc: &BoxConstraints,
        _data: &BotData,
        _env: &Env,
    ) -> Size {
        bc.debug_check("Image");
        // This is meant to expand to fill available space.
        bc.max()
    }

    fn paint(&mut self, ctx: &mut PaintCtx, data: &BotData, _env: &Env) {
        let parent_x = ctx.size().width;
        let parent_y = ctx.size().height;
        let parent_min = parent_x.min(parent_y);

        let clip_rect = ctx.size().to_rect();
        ctx.clip(clip_rect);
        ctx.solid_brush(Color::rgba(0.0, 0.0, 0.0, 1.0));

        let (image_box, image_x, image_y, image_size) = (
            &self.image_vis.image,
            &self.image_vis.center_x,
            &self.image_vis.center_y,
            &self.image_vis.rad,
        );
        let offset_matrix = proprieties_to_affine(
            parent_min,
            parent_x,
            parent_y,
            data.view,
            data.center_x,
            data.center_y,
            image_size.to_owned(),
            image_x.to_owned(),
            image_y.to_owned(),
            image_box.width(),
        );
        ctx.with_save(|ctx| {
            let piet_image = &image_box.to_image(ctx.render_ctx);
            ctx.transform(offset_matrix);
            ctx.draw_image(piet_image, image_box.size().to_rect(), self.interpolation);
        });

        let mut skip = true;
        if let Some(image_qual) = &self.image_qual {
            let (image_box, image_x, image_y, image_size) = (
                &image_qual.image,
                image_qual.center_x.to_owned(),
                image_qual.center_y.to_owned(),
                image_qual.rad.to_owned(),
            );
            if image_size / data.view > 0.1 {
                let offset_matrix = proprieties_to_affine(
                    parent_min,
                    parent_x,
                    parent_y,
                    data.view,
                    data.center_x,
                    data.center_y,
                    image_size,
                    image_x,
                    image_y,
                    image_box.width(),
                );
                ctx.with_save(|ctx| {
                    let piet_image = &image_box.to_image(ctx.render_ctx);

                    ctx.transform(offset_matrix);
                    ctx.draw_image(piet_image, image_box.size().to_rect(), self.interpolation);
                });
                if !((data.view / image_size).abs() < 0.2) {
                    skip = false;
                };
            }
        };

        if skip {
            let (image_box, image_x, image_y, image_size) = (
                &self.image_vis_a.image,
                &self.image_vis_a.center_x,
                &self.image_vis_a.center_y,
                &self.image_vis_a.rad,
            );
            let offset_matrix = proprieties_to_affine(
                parent_min,
                parent_x,
                parent_y,
                data.view,
                data.center_x,
                data.center_y,
                image_size.to_owned(),
                image_x.to_owned(),
                image_y.to_owned(),
                image_box.width(),
            );
            ctx.with_save(|ctx| {
                let piet_image = &image_box.to_image(ctx.render_ctx);
                ctx.transform(offset_matrix);
                ctx.draw_image(piet_image, image_box.size().to_rect(), self.interpolation);
            });
        }

        if let Some(mouse_start) = self.mouse_down {
            if let Some(mouse_end) = self.mouse_current {
                let brush = ctx.solid_brush(Color::rgb8(254, 254, 254));
                let shape = Rect::new(mouse_start.x, mouse_start.y, mouse_end.x, mouse_end.y);
                ctx.stroke(shape, &brush, 1.0)
            }
        }
    }
}

fn proprieties_to_affine(
    parent_min: f64,
    parent_x: f64,
    parent_y: f64,
    zoom_view: f64,
    zoom_x: f64,
    zoom_y: f64,
    image_size: f64,
    image_x: f64,
    image_y: f64,
    image_width: usize,
) -> Affine {
    let pixels_per_view = parent_min / (zoom_view * 2.0);

    let pixels_per_local_view = image_width as f64 / (image_size * 2.0);

    let scale_local_to_screen = pixels_per_view / pixels_per_local_view;

    let scale_y = scale_local_to_screen;
    let scale_x = scale_local_to_screen;

    let translate_center_x = (parent_x - parent_min) / 2.0;
    let translate_center_y = (parent_y - parent_min) / 2.0;

    let origin_x =
        translate_center_x - (image_size - zoom_view) * pixels_per_view - zoom_x * pixels_per_view
            + image_x * pixels_per_view;
    let origin_y =
        translate_center_y - (image_size - zoom_view) * pixels_per_view - zoom_y * pixels_per_view
            + image_y * pixels_per_view;

    Affine::new([scale_x, 0., 0., scale_y, origin_x, origin_y])
}

fn make_center() -> impl Widget<BotData> {
    Flex::column()
        .cross_axis_alignment(CrossAxisAlignment::Start)
        .with_child(Label::new("Center:"))
        .with_default_spacer()
        .with_child(
            TextBox::new()
                .with_formatter(ParseFormatter::new())
                .lens(BotData::center_x)
                .fix_width(60.0),
        )
        .with_default_spacer()
        .with_child(
            TextBox::new()
                .with_formatter(ParseFormatter::new())
                .lens(BotData::center_y)
                .fix_width(60.0),
        )
}

fn make_zoom() -> impl Widget<BotData> {
    Flex::column()
        .cross_axis_alignment(CrossAxisAlignment::Start)
        .with_child(Label::new("View size/scale:"))
        .with_default_spacer()
        .with_child(
            TextBox::new()
                .with_formatter(ParseFormatter::new())
                .lens(BotData::view)
                .fix_width(60.0),
        )
}

fn make_colors() -> impl Widget<BotData> {
    Flex::column()
        .cross_axis_alignment(CrossAxisAlignment::Start)
        .with_child(Label::new("Colors:"))
        .with_default_spacer()
        .with_child(
            TextBox::new()
                .with_formatter(ParseFormatter::new())
                .lens(BotData::max)
                .fix_width(60.0),
        )
        .with_default_spacer()
        .with_child(
            TextBox::new()
                .with_formatter(ParseFormatter::new())
                .lens(BotData::min)
                .fix_width(60.0),
        )
        .with_default_spacer()
        .with_child(RadioGroup::new(COLOUR_SCALE_OPTIONS.to_vec()).lens(BotData::colour_scale))
}

fn make_howto() -> impl Widget<BotData> {
    Flex::column()
        .cross_axis_alignment(CrossAxisAlignment::Start)
        .with_child(Label::new("How too:"))
        .with_default_spacer()
        .with_child(Label::new("Pan: ctl + drag left mouse"))
        .with_default_spacer()
        .with_child(Label::new("Zoom: Scroll mouse wheel"))
        .with_default_spacer()
        .with_child(Label::new("Region Zoom: left click and drag over region"))
        .with_default_spacer()
        .with_child(Label::new("Recenter: left mouse click"))
        .with_default_spacer()
}

fn settings_builder() -> impl Widget<BotData> {
    Flex::row()
        .cross_axis_alignment(CrossAxisAlignment::Start)
        .with_child(make_howto())
        .with_default_spacer()
        .with_child(make_center())
        .with_default_spacer()
        .with_child(make_zoom())
        .with_default_spacer()
        .with_child(make_colors())
        .with_default_spacer()
        .padding(10.0)
}

fn ui_builder() -> impl Widget<BotData> {
    let bot = DruidBotWidget::new();
    Flex::column()
        .with_child(settings_builder())
        .with_flex_child(bot, 1.0)
}

fn background_processing(event_sink: druid::ExtEventSink, receiver: Receiver<NewBot>) {
    loop {
        let mut next = receiver.recv().unwrap();
        loop {
            if let Ok(posable) = receiver.try_recv() {
                next = posable;
            } else {
                break;
            }
        }

        let bot = do_bot(next);
        event_sink
            .submit_command(FINISH_SLOW_FUNCTION, Arc::new(bot), Target::Auto)
            .expect("command failed to submit");
    }
}

struct Delegate {
    send: Sender<NewBot>,
}

impl AppDelegate<BotData> for Delegate {
    fn command(
        &mut self,
        _ctx: &mut DelegateCtx,
        _target: Target,
        cmd: &Command,
        data: &mut BotData,
        _env: &Env,
    ) -> Handled {
        if let Some(bot) = cmd.get(REQUEST_SLOW_FUNCTION) {
            self.send.send(*bot).unwrap();
            Handled::Yes
        } else if let Some(bot) = cmd.get(FINISH_SLOW_FUNCTION) {
            data.bot_qual = Some(bot.clone());
            Handled::Yes
        } else {
            Handled::No
        }
    }
}

fn main() {
    let num = num_cpus::get();
    rayon::ThreadPoolBuilder::new()
        .num_threads(num - 1)
        .build_global()
        .unwrap();
    let matches = App::new("My Super Program")
        .version("1.0")
        .author("Will")
        .about("Does awesome things")
        .arg(
            Arg::with_name("resolution")
                .short("r")
                .long("res")
                .value_name("RES")
                .help("Number of pixels")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("x_pos")
                .short("x")
                .long("x_pos")
                .value_name("X")
                .help("real part of the center")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("y_pos")
                .short("y")
                .long("y_pos")
                .value_name("Y")
                .help("Complex part of the center")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("scale")
                .short("s")
                .long("scale")
                .value_name("scale")
                .help("Magnitude of the complex pain visible around the center")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("gui")
                .short("g")
                .long("gui")
                .value_name("GUI")
                .help("Use a gui")
                .takes_value(false),
        )
        .get_matches();

    // Gets a value for config if supplied by user, or defaults to "default.conf"
    let num_str = matches.value_of("resolution");
    let resolution = match num_str {
        None => 21,
        Some(s) => match s.parse::<i32>() {
            Ok(n) => n,
            Err(_) => 21,
        },
    };

    let x_str = matches.value_of("x_pos");
    let x_center = match x_str {
        None => 0.0,
        Some(s) => match s.parse::<f64>() {
            Ok(n) => n,
            Err(_) => 0.0,
        },
    };
    let y_str = matches.value_of("y_pos");
    let y_center = match y_str {
        None => 0.0,
        Some(s) => match s.parse::<f64>() {
            Ok(n) => {
                println!("Centered at Im: {}.", n);
                n
            }
            Err(_) => {
                println!("That's not a number! {}", s);
                0.0
            }
        },
    };
    let scale_str = matches.value_of("scale");
    let scale = match scale_str {
        None => 2.0,
        Some(s) => match s.parse::<f64>() {
            Ok(n) => {
                println!("Visible magnitude: {}.", n);
                n
            }
            Err(_) => {
                println!("That's not a number! {}", s);
                2.0
            }
        },
    };

    let gui = matches.is_present("gui");
    if gui {
        let main_window = WindowDesc::new(ui_builder()).title(
            LocalizedString::new("Mandelbrot viewer").with_placeholder("Mandelbrot viewer!"),
        );
        let data = BotData {
            bot_qual: None,
            view_box: Rect::new(-2.0, -2.0, 2.0, 2.0),
            //center: Point::new(0.0, 0.0),
            center_x: x_center,
            center_y: y_center,
            view: scale,
            min: 0,
            max: 1000,
            colour_scale: ColourScale::Log,
            data_processing: false,
        };
        let launcher = AppLauncher::with_window(main_window);
        let event_sink = launcher.get_external_handle();
        let (tx, rx): (Sender<NewBot>, Receiver<NewBot>) = mpsc::channel();
        // `background_processing` takes `NewBot`s from a receiver and slowly turns
        // them in to `Bot`s which it sends a Arc of to event_sink which is
        // received by delegate.
        thread::spawn(move || background_processing(event_sink, rx));
        launcher
            // Delegate gets called when there is a new message
            // This could be a request from the rest of the program in which case it forwards it over its
            // Sender to `background_processing` or it could be from `background_process` in which case it
            // modifies the druid `data` as `background_processing` can not directly change the program data.
            .delegate(Delegate { send: tx })
            .log_to_console()
            .launch(data)
            .expect("launch failed");
    } else {
        let bot = do_bot(NewBot {
            sub_divs: resolution,
            x_center,
            y_center,
            scale,
            max_iter: 5000,
        });
        print_bot(bot);
    }
}
